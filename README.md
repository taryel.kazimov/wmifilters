# Custom Filters

## Apply policy to LAPTOPS only

"Select * from Win32_ComputerSystem WHERE (PCSystemType = 2)"

## Apply policy to VMWare virtual machines only:

SELECT Model FROM Win32_ComputerSystem WHERE Model = “VMWare Virtual Platform”

## To Filter based on subnets

Select * FROM Win32_IP4RouteTable WHERE (Mask='255.255.255.255'AND (Destination Like '192.168.1.%' OR Destination Like '192.168.2.%' OR Destination Like '192.168.3.%' OR Destination Like '192.168.4.%'))

## To Filter any Windows server OS – 64bit

select * from Win32_OperatingSystem where (ProductType = "2") OR (ProductType = "3") AND  OSArchitecture = "64-bit"

## To apply policy in selected day of the week

select DayOfWeek from Win32_LocalTime where DayOfWeek = 1


# DESKTOPS

## ANY WINDOWS DESKTOP OS

Any Windows Desktop OS – Version 1 
select * from Win32_OperatingSystem WHERE ProductType = "1"
Any Windows Desktop OS – Version 2 (better for Win7 sometimes) 
select * from Win32_OperatingSystem WHERE (ProductType <> "2") AND (ProductType <> "3")
Any Windows Desktop OS – 32-bit 
select * from Win32_OperatingSystem WHERE ProductType = "1" AND NOT OSArchitecture = "64-bit"
Any Windows Desktop OS – 64-bit 
select * from Win32_OperatingSystem WHERE ProductType = "1" AND OSArchitecture = "64-bit"

## WINDOWS XP

Windows XP 
select * from Win32_OperatingSystem WHERE (Version like "5.1%" or Version like "5.2%") AND ProductType="1"
Windows XP – 32-bit 
select * from Win32_OperatingSystem WHERE (Version like "5.1%" or Version like "5.2%") AND ProductType="1" AND NOT OSArchitecture = "64-bit"
Windows XP – 64-bit 
select * from Win32_OperatingSystem WHERE (Version like "5.1%" or Version like "5.2%") AND ProductType="1" AND OSArchitecture = "64-bit"

## WINDOWS VISTA

Windows Vista 
select * from Win32_OperatingSystem WHERE Version like "6.0%" AND ProductType="1"
Windows Vista – 32-bit 
select * from Win32_OperatingSystem WHERE Version like "6.0%" AND ProductType="1" AND NOT OSArchitecture = "64-bit"
Windows Vista – 64-bit 
select * from Win32_OperatingSystem WHERE Version like "6.0%" AND ProductType="1" AND OSArchitecture = "64-bit"

## WINDOWS 7

Windows 7 
select * from Win32_OperatingSystem WHERE Version like "6.1%" AND ProductType="1"
Windows 7 – 32-bit 
select * from Win32_OperatingSystem WHERE Version like "6.1%" AND ProductType="1" AND NOT OSArchitecture = "64-bit"
Windows 7 – 64-bit 
select * from Win32_OperatingSystem WHERE Version like "6.1%" AND ProductType="1" AND OSArchitecture = "64-bit"

## WINDOWS 8

Windows 8 
select * from Win32_OperatingSystem WHERE Version like "6.2%" AND ProductType="1"
Windows 8 – 32-bit 
select * from Win32_OperatingSystem WHERE Version like "6.2%" AND ProductType="1" AND NOT OSArchitecture = "64-bit"
Windows 8 – 64-bit 
select * from Win32_OperatingSystem WHERE Version like "6.2%" AND ProductType="1" AND OSArchitecture = "64-bit"

## WINDOWS 8.1

Windows 8.1 
select * from Win32_OperatingSystem WHERE Version like "6.3%" AND ProductType="1"
Windows 8.1 – 32-bit 
select * from Win32_OperatingSystem WHERE Version like "6.3%" AND ProductType="1" AND NOT OSArchitecture = "64-bit"
Windows 8.1 – 64-bit 
select * from Win32_OperatingSystem WHERE Version like "6.3%" AND ProductType="1" AND OSArchitecture = "64-bit"


# SERVERS

## ANY WINDOWS SERVER OS

Any Windows Server OS 
select * from Win32_OperatingSystem where (ProductType = "2") OR (ProductType = "3")
Any Windows Server OS – 32-bit 
select * from Win32_OperatingSystem where (ProductType = "2") OR (ProductType = "3") AND NOT OSArchitecture = "64-bit"
Any Windows Server OS – 64-bit 
select * from Win32_OperatingSystem where (ProductType = "2") OR (ProductType = "3") AND OSArchitecture = "64-bit"
Any Windows Server – Domain Controller 
select * from Win32_OperatingSystem where (ProductType = "2")
Any Windows Server – Domain Controller – 32-bit 
select * from Win32_OperatingSystem where (ProductType = "2") AND NOT OSArchitecture = "64-bit"
Any Windows Server – Domain Controller – 64-bit 
select * from Win32_OperatingSystem where (ProductType = "2") AND OSArchitecture = "64-bit"
Any Windows Server – Non-Domain Controller 
select * from Win32_OperatingSystem where (ProductType = "3")
Any Windows Server – Non- Domain Controller – 32-bit 
select * from Win32_OperatingSystem where (ProductType = "3") AND NOT OSArchitecture = "64-bit"
Any Windows Server – Non-Domain Controller – 64-bit 
select * from Win32_OperatingSystem where (ProductType = "3") AND OSArchitecture = "64-bit"

## WINDOWS SERVER 2003

Windows Server 2003 – DC 
select * from Win32_OperatingSystem WHERE Version like "5.2%" AND ProductType="2"
Windows Server 2003 – non-DC 
select * from Win32_OperatingSystem WHERE Version like "5.2%" AND ProductType="3"
Windows Server 2003 – 32-bit – DC 
select * from Win32_OperatingSystem WHERE Version like "5.2%" AND ProductType="2" AND NOT OSArchitecture = "64-bit"
Windows Server 2003 – 32-bit – non-DC 
select * from Win32_OperatingSystem WHERE Version like "5.2%" AND ProductType="3" AND NOT OSArchitecture = "64-bit"
Windows Server 2003 – 64-bit – DC 
select * from Win32_OperatingSystem WHERE Version like "5.2%" AND ProductType="2" AND OSArchitecture = "64-bit"
Windows Server 2003 – 64-bit – non-DC 
select * from Win32_OperatingSystem WHERE Version like "5.2%" AND ProductType="3" AND OSArchitecture = "64-bit"

## WINDOWS SERVER 2003 R2

Windows Server 2003 R2 – DC 
select * from Win32_OperatingSystem WHERE Version like "5.2.3%" AND ProductType="2"
Windows Server 2003 R2 – non-DC 
select * from Win32_OperatingSystem WHERE Version like "5.2.3%" AND ProductType="3"
Windows Server 2003 R2 – 32-bit – DC 
select * from Win32_OperatingSystem WHERE Version like "5.2.3%" AND ProductType="2" AND NOT OSArchitecture = "64-bit"
Windows Server 2003 R2 – 32-bit – non-DC 
select * from Win32_OperatingSystem WHERE Version like "5.2.3%" AND ProductType="3" AND NOT OSArchitecture = "64-bit"
Windows Server 2003 R2 – 64-bit – DC 
select * from Win32_OperatingSystem WHERE Version like "5.2.3%" AND ProductType="2" AND OSArchitecture = "64-bit"
Windows Server 2003 R2 – 64-bit – non-DC 
select * from Win32_OperatingSystem WHERE Version like "5.2.3%" AND ProductType="3" AND OSArchitecture = "64-bit"

## WINDOWS SERVER 2008

Windows Server 2008 – DC 
select * from Win32_OperatingSystem WHERE Version like "6.0%" AND ProductType="2"
Windows Server 2008 – non-DC 
select * from Win32_OperatingSystem WHERE Version like "6.0%" AND ProductType="3"
Windows Server 2008 – 32-bit – DC 
select * from Win32_OperatingSystem WHERE Version like "6.0%" AND ProductType="2" AND NOT OSArchitecture = "64-bit"
Windows Server 2008 – 32-bit – non-DC 
select * from Win32_OperatingSystem WHERE Version like "6.0%" AND ProductType="3" AND NOT OSArchitecture = "64-bit"
Windows Server 2008 – 64-bit – DC 
select * from Win32_OperatingSystem WHERE Version like "6.0%" AND ProductType="2" AND OSArchitecture = "64-bit"
Windows Server 2008 – 64-bit – non-DC 
select * from Win32_OperatingSystem WHERE Version like "6.0%" AND ProductType="3" AND OSArchitecture = "64-bit"

## WINDOWS SERVER 2008 R2

Windows Server 2008 R2 – 64-bit – DC 
select * from Win32_OperatingSystem WHERE Version like "6.1%" AND ProductType="2"
Windows Server 2008 R2 – 64-bit – non-DC 
select * from Win32_OperatingSystem WHERE Version like "6.1%" AND ProductType="3"

## WINDOWS SERVER 2012

Windows Server 2012 – 64-bit – DC 
select * from Win32_OperatingSystem WHERE Version like "6.2%" AND ProductType="2"
Windows Server 2012 – 64-bit – non-DC 
select * from Win32_OperatingSystem WHERE Version like "6.2%" AND ProductType="3"

## WINDOWS SERVER 2012 R2

Windows Server 2012 R2 – 64-bit – DC 
select * from Win32_OperatingSystem WHERE Version like "6.3%" AND ProductType="2"
Windows Server 2012 R2 – 64-bit – non-DC 
select * from Win32_OperatingSystem WHERE Version like "6.3%" AND ProductType="3"
